var dbConfig= require('../configuration/databaseConfig');

/**
 * Iniciacion y configuración del modulo de Knex, necesario para
 * utilizar el ORM Bookshelf.js
 */
module.exports.knex = require('knex')({
  client: 'mysql',
  connection: dbConfig
});