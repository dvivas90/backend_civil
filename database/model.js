var con=require('./connection');

var bookshelf = require('bookshelf')(con.knex);
var encryptColumns = require('bookshelf-encrypt-columns');
 
bookshelf.plugin(encryptColumns, {
  cipher: 'aes-128-ecb',
  key: process.argv[2]
});

/**
 * @deprecated
 * Definición del modelo Rol
 */
var Rol = bookshelf.Model.extend({
  tableName: 'ROL',
  idAttribute: 'idROL'
});

var Actividad = bookshelf.Model.extend({
  tableName: 'ACTIVIDAD',
  idAttribute: 'idACTIVIDAD'
});

/**
 * Definición del modelo Usuario
 */
var Usuario=bookshelf.Model.extend({
  tableName:'USUARIO',
  idAttribute:'idUSUARIO',
  encryptedColumns: ['password'],
  rol:function(){
    return this.belongsTo(Rol, 'idROL');
  }
});


/**
 * Definición del modelo Cliente
 */
var Cliente=bookshelf.Model.extend({
  tableName:'CLIENTE',
  idAttribute:'idCLIENTE'
});

var Proveedort=bookshelf.Model.extend({
  tableName:'PROVEEDORT',
  idAttribute:'idPROVEEDORT'
});

/**
 * @deprecated
 * Definición del modelo Equipo
 */
var Equipo = bookshelf.Model.extend({
  tableName: 'EQUIPO',
  idAttribute: 'idEQUIPO'
});

/**
 * Definición del modelo Proveedor
 */
var Proveedor = bookshelf.Model.extend({
  tableName: 'PROVEEDOR',
  idAttribute: 'idPROVEEDOR'

});

/**
 * Definición del modelo obra
 */
var Obra = bookshelf.Model.extend({
  tableName: 'OBRA',
  idAttribute: 'idOBRA'
});

/**
 * Definición del modelo propietario
 */
var Propietario = bookshelf.Model.extend({
  tableName: 'PROPIETARIO',
  idAttribute: 'idPROPIETARIO'
});

/**
 * Definición del modelo Material
 */
var Material = bookshelf.Model.extend({
  tableName: 'MATERIAL',
  idAttribute: 'idMATERIAL'
});

/**
 * Definición del modelo Control Materiales
 */
var ControlMateriales = bookshelf.Model.extend({
  tableName: 'CONTROL_MATERIALES',
  idAttribute: 'idCONTROL_MATERIALES',
  obra:function(){
    return this.belongsTo(Obra, 'idOBRA');
  },
  cliente:function(){
    return this.belongsTo(Cliente, 'idCLIENTE');
  }
  ,
  material:function(){
    return this.belongsTo(Material, 'idMATERIAL');
  }
  ,
  proveedor:function(){
    return this.belongsTo(Proveedor, 'idPROVEEDOR');
  },
  proveedort:function(){
    return this.belongsTo(Proveedort, 'idPROVEEDORT');
  },
  usuario:function(){
    return this.belongsTo(Usuario, 'idUSUARIO');
  }

});

/**
 * Definición del modelo Reporte Equipo
 */
var ReporteEquipo=bookshelf.Model.extend({
  tableName:'REPORTE_EQUIPO',
  idAttribute:'idREPORTE_EQUIPO',
  usuario:function(){
    return this.belongsTo(Usuario,'idUSUARIO');
  },
  obra:function(){
    return this.belongsTo(Obra, 'idOBRA');
  },
  propietario:function(){
    return this.belongsTo(Propietario, 'idPROPIETARIO');
  }
  ,
  equipo:function(){
    return this.belongsTo(Equipo, 'idEQUIPO');
  }
});


var ObraProveedort=bookshelf.Model.extend({
  tableName:'OBRAPROVEEDORT',
  idAttribute:'idOBRAPROVEEDORT',
  obra:function(){
    return this.belongsTo(Obra, 'idOBRA');
  }
  ,
  proveedort:function(){
    return this.belongsTo(Proveedort, 'idPROVEEDORT');
  }
});


/**
 * Definición del modelo Detalle Reporte Equipo
 */
var DetReporteEquipo=bookshelf.Model.extend({
  tableName:'DET_REPORTE_EQUIPO',
  idAttribute:'idDET_REPORTE_EQUIPO',
  reporteEquipo:function(){
    return this.belongsTo(ReporteEquipo, 'idREPORTE_EQUIPO')
  },
  actividad:function(){
    return this.belongsTo(Actividad, 'idACTIVIDAD');
  }
});
module.exports = {
  Proveedort:Proveedort,
  Actividad:Actividad,
  Rol:Rol,
  Usuario:Usuario,
  Cliente:Cliente,
  Equipo:Equipo,
  Proveedor:Proveedor,
  Obra:Obra,
  Propietario:Propietario,
  Material:Material,
  ControlMateriales:ControlMateriales,
  ReporteEquipo:ReporteEquipo, 
  DetReporteEquipo:DetReporteEquipo, 
  ObraProveedort:ObraProveedort,
  knex:bookshelf.knex
}

