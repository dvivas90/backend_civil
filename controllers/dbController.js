var model = require('../database/model');
var dateUtil=require('../utilities/dateUtil');
var config = require('../configuration/appConfig');



///INSERTS

/**
 * Inserta una nueva obra en la BD
 * @param {Object} datosobra información necesaria para crear una nueva obra
 * @returns {Promise} modelo del nuevo recorrido insertado
 */
module.exports.insertarObra = (datosobra) => {
  return model.Obra.forge(datosobra).save();
}


module.exports.editarObra = (idOBRA, campos) => {
  return model.Obra.forge({ idOBRA: idOBRA }).save(campos);
}

module.exports.eliminarObra = (idOBRA) => {
  return model.Obra.forge({ idOBRA: idOBRA }).destroy();
}

module.exports.consultarObras=()=>{
  return model.Obra
  .query(function (qb) {
    qb.limit(100);
  }).orderBy('idOBRA', 'DESC').fetchAll();

}

module.exports.consultarObrasFiltros=(filtros)=>{
  return model.Obra
  .query(function (qb) {
    qb.limit(100);
    qb.where('nomOBRA', 'like', '%'+filtros.nomOBRA+'%');
    
  }).fetchAll();
}

module.exports.insertarCliente = (datosCliente) => {
  return model.Cliente.forge(datosCliente).save();
}

module.exports.editarCliente = (idCLIENTE, campos) => {
  return model.Cliente.forge({ idCLIENTE: idCLIENTE }).save(campos);
}

module.exports.eliminarCliente = (idCLIENTE) => {
  return model.Cliente.forge({ idCLIENTE: idCLIENTE }).destroy();
}

module.exports.consultarClientes=()=>{
  return model.Cliente
  .query(function (qb) {
    qb.limit(100);
  }).orderBy('idCLIENTE', 'DESC').fetchAll();

}

module.exports.consultarClientesFiltros=(filtros)=>{
  return model.Cliente
  .query(function (qb) {
    qb.limit(100);
    qb.where('nomCLIENTE', 'like', '%'+filtros.nomCLIENTE+'%');
    
  }).fetchAll();
}

module.exports.insertarProveedor = (datosProveedor) => {
  return model.Proveedor.forge(datosProveedor).save();
}

module.exports.editarProveedor = (idPROVEEDOR, campos) => {
  return model.Proveedor.forge({ idPROVEEDOR: idPROVEEDOR }).save(campos);
}

module.exports.eliminarProveedor = (idPROVEEDOR) => {
  return model.Proveedor.forge({ idPROVEEDOR: idPROVEEDOR }).destroy();
}

module.exports.consultarProveedores=()=>{
  return model.Proveedor
  .query(function (qb) {
    qb.limit(100);
  }).orderBy('idPROVEEDOR', 'DESC').fetchAll();

}

module.exports.consultarProveedoresFiltros=(filtros)=>{
  return model.Proveedor
  .query(function (qb) {
    qb.limit(100);
    qb.where('nomPROVEEDOR', 'like', '%'+filtros.nomPROVEEDOR+'%');
    
  }).fetchAll();
}



module.exports.consultarEquiposFiltros=(filtros)=>{
  return model.Equipo
  .query(function (qb) {
    qb.limit(100);
    qb.where('nomEQUIPO', 'like', '%'+filtros.nomEQUIPO+'%');
    
  }).fetchAll();
}

module.exports.insertarEquipo = (datosEquipo) => {
  return model.Equipo.forge(datosEquipo).save();
}

module.exports.editarEquipo = (idEQUIPO, campos) => {
  return model.Equipo.forge({ idEQUIPO: idEQUIPO }).save(campos);
}

module.exports.eliminarEquipo= (idEQUIPO) => {
  return model.Equipo.forge({ idEQUIPO: idEQUIPO }).destroy();
}

module.exports.consultarEquipos=()=>{
  return model.Equipo
  .query(function (qb) {
    qb.limit(100);
  }).orderBy('idEQUIPO', 'DESC').fetchAll();

}


module.exports.consultarMaterialesFiltros=(filtros)=>{
  return model.Material
  .query(function (qb) {
    qb.limit(100);
    qb.where('nomMATERIAL', 'like', '%'+filtros.nomMATERIAL+'%');
    
  }).fetchAll();
}

module.exports.insertarMaterial = (datosMaterial) => {
  return model.Material.forge(datosMaterial).save();
}

module.exports.editarMaterial = (idMATERIAL, campos) => {
  return model.Material.forge({ idMATERIAL: idMATERIAL }).save(campos);
}

module.exports.eliminarMaterial= (idMATERIAL) => {
  return model.Material.forge({ idMATERIAL: idMATERIAL }).destroy();
}

module.exports.consultarMateriales=()=>{
  return model.Material
  .query(function (qb) {
    qb.limit(100);
  }).orderBy('idMATERIAL', 'DESC').fetchAll();

}


module.exports.consultarPropietariosFiltros=(filtros)=>{
  return model.Propietario
  .query(function (qb) {
    qb.limit(100);
    qb.where('nomPROPIETARIO', 'like', '%'+filtros.nomPROPIETARIO+'%');
    
  }).fetchAll();
}

module.exports.insertarPropietario = (datosPropietario) => {
  return model.Propietario.forge(datosPropietario).save();
}

module.exports.editarPropietario = (idPROPIETARIO, campos) => {
  return model.Propietario.forge({ idPROPIETARIO: idPROPIETARIO }).save(campos);
}

module.exports.eliminarPropietario= (idPROPIETARIO) => {
  return model.Propietario.forge({ idPROPIETARIO: idPROPIETARIO }).destroy();
}

module.exports.consultarPropietarios=()=>{
  return model.Propietario
  .query(function (qb) {
    qb.limit(100);
  }).orderBy('idPROPIETARIO', 'DESC').fetchAll();

}


module.exports.consultarReportesEquipoFiltros=(filtros)=>{
  return model.ReporteEquipo
  .query(function (qb) {
    qb.limit(100);
    if(filtros.idREPORTE_EQUIPO!=null){
    qb.where('idREPORTE_EQUIPO', '=', filtros.idREPORTE_EQUIPO);
    }
    if (filtros.fechaDesde!=null && filtros.fechaHasta!=null ){
    qb.whereBetween('fecha', [filtros.fechaDesde, filtros.fechaHasta]);
    }
  }).orderBy('idREPORTE_EQUIPO', 'DESC').fetchAll({withRelated: ['equipo','obra','propietario','usuario']});
}

module.exports.insertarReporteEquipo = (datosReporteEquipo) => {
  return model.ReporteEquipo.forge(datosReporteEquipo).save();
}

module.exports.editarReporteEquipo = (idREPORTE_EQUIPO, campos) => {
  return model.ReporteEquipo.forge({ idREPORTE_EQUIPO: idREPORTE_EQUIPO }).save(campos);
}

module.exports.eliminarReporteEquipo= (idREPORTE_EQUIPO) => {
  return model.ReporteEquipo.forge({ idREPORTE_EQUIPO: idREPORTE_EQUIPO }).destroy();
}

module.exports.consultarReporteEquipos=()=>{
  return model.ReporteEquipo
  .query(function (qb) {
    qb.limit(100);
  }).orderBy('idREPORTE_EQUIPO', 'DESC').fetchAll({withRelated: ['equipo','obra','propietario','usuario']});

}

module.exports.insertarDetReporteEquipo = (datosDetReporteEquipo) => {
  return model.DetReporteEquipo.forge(datosDetReporteEquipo).save();
}

module.exports.editarDetReporteEquipo = (idDET_REPORTE_EQUIPO, campos) => {
  return model.DetReporteEquipo.forge({ idDET_REPORTE_EQUIPO: idDET_REPORTE_EQUIPO }).save(campos);
}

module.exports.eliminarDetReporteEquipo= (idREPORTE_EQUIPO) => {
  return model.DetReporteEquipo.where({ idREPORTE_EQUIPO: idREPORTE_EQUIPO }).destroy();
}

module.exports.consultarDetReporteEquipo=(idREPORTE_EQUIPO)=>{
  return model.DetReporteEquipo
  .where({idREPORTE_EQUIPO:idREPORTE_EQUIPO})
  .query(function (qb) {
    qb.limit(100);
  }).orderBy('idDET_REPORTE_EQUIPO', 'DESC').fetchAll({withRelated: ['reporteEquipo','actividad']});

}

module.exports.consultarControlMaterialesFiltros=(filtros)=>{

  return model.ControlMateriales
  .query(function (qb) {
    qb.limit(100);
    if(filtros.idCONTROL_MATERIALES!=null){
    qb.where('idCONTROL_MATERIALES', '=', filtros.idCONTROL_MATERIALES);
    }
    if (filtros.fechaDesde!=null && filtros.fechaHasta!=null ){
    qb.whereBetween('fecha', [filtros.fechaDesde, filtros.fechaHasta]);
    }
  }).orderBy('idCONTROL_MATERIALES', 'DESC').fetchAll({withRelated: ['cliente','obra','material','proveedor','proveedort','usuario']});
 
}

module.exports.insertarControlMateriales = (datosControlMateriales) => {
  return model.ControlMateriales.forge(datosControlMateriales).save();
}

module.exports.editarControlMateriales = (idCONTROL_MATERIALES, campos) => {
  return model.ControlMateriales.forge({ idCONTROL_MATERIALES: idCONTROL_MATERIALES }).save(campos);
}

module.exports.eliminarControlMateriales= (idCONTROL_MATERIALES) => {
  return model.ControlMateriales.forge({ idCONTROL_MATERIALES: idCONTROL_MATERIALES }).destroy();
}


module.exports.consultarControlMateriales=()=>{
  return model.ControlMateriales
  .query(function (qb) {
    qb.limit(100);
  }).orderBy('idCONTROL_MATERIALES', 'DESC').fetchAll({withRelated: ['cliente','obra','material','proveedor','proveedort','usuario']});

}

module.exports.consultarUsuarios=()=>{
  return model.Usuario
  .query(function (qb) {
    qb.limit(100);
  }).orderBy('idUSUARIO', 'DESC').fetchAll({withRelated: ['rol']});

}

module.exports.consultarRoles=()=>{
  return model.Rol
  .query(function (qb) {
    qb.limit(100);
  }).orderBy('idROL', 'DESC').fetchAll();

}

module.exports.consultarUsuarioFiltros=(filtros)=>{

  return model.Usuario
  .query(function (qb) {
    qb.limit(100);
    if(filtros.login!=null){
    qb.where('login', '=', filtros.login);
    }

    if(filtros.nombre!=null){
      qb.where('nombre', 'like', '%'+filtros.nombre+'%');
    }

    if (filtros.password!=null){
    qb.where('password', '=', filtros.password);
    }
  }).orderBy('idUSUARIO', 'DESC').fetchAll({withRelated: ['rol']});
 
}

module.exports.insertarUsuario = (datosUsuario) => {
  return model.Usuario.forge(datosUsuario).save();
}

module.exports.editarUsuario = (idUSUARIO, campos) => {
  return model.Usuario.forge({ idUSUARIO: idUSUARIO }).save(campos);
}

module.exports.eliminarUsuario =  (idUSUARIO) => {
  return model.Usuario.forge({ idUSUARIO: idUSUARIO }).destroy();
}

module.exports.insertarActividad = (datosActividad) => {
  return model.Actividad.forge(datosActividad).save();
}

module.exports.editarActividad = (idACTIVIDAD, campos) => {
  return model.Actividad.forge({ idACTIVIDAD: idACTIVIDAD }).save(campos);
}

module.exports.eliminarActividad = (idACTIVIDAD) => {
  return model.Actividad.forge({ idACTIVIDAD: idACTIVIDAD }).destroy();
}

module.exports.consultarActividades=()=>{
  return model.Actividad
  .query(function (qb) {
    qb.limit(100);
  }).orderBy('idACTIVIDAD', 'DESC').fetchAll();

}

module.exports.consultarActividadesFiltros=(filtros)=>{
  return model.Actividad
  .query(function (qb) {
    qb.limit(100);
    qb.where('nomACTIVIDAD', 'like', '%'+filtros.nomACTIVIDAD+'%');
    
  }).fetchAll();
}

module.exports.insertarProveedort = (datosProveedort) => {
  return model.Proveedort.forge(datosProveedort).save();
}

module.exports.editarProveedort = (idPROVEEDORT, campos) => {
  return model.Proveedort.forge({ idPROVEEDORT: idPROVEEDORT }).save(campos);
}

module.exports.eliminarProveedort = (idPROVEEDORT) => {
  return model.Proveedort.forge({ idPROVEEDORT: idPROVEEDORT }).destroy();
}

module.exports.consultarProveedorest=()=>{
  return model.Proveedort
  .query(function (qb) {
    qb.limit(100);
  }).orderBy('idPROVEEDORT', 'DESC').fetchAll();

}

module.exports.consultarProveedorestFiltros=(filtros)=>{
  return model.Proveedort
  .query(function (qb) {
    qb.limit(100);
    qb.where('nomPROVEEDORT', 'like', '%'+filtros.nomACTIVIDAD+'%');
    
  }).fetchAll();
  
}



module.exports.insertarObraProveedort = (datosObraProveedort) => {
  return model.ObraProveedort.forge(datosObraProveedort).save();
}

module.exports.editarObraProveedort = (idOBRAPROVEEDORT, campos) => {
  return model.ObraProveedort.forge({ idOBRAPROVEEDORT: idOBRAPROVEEDORT }).save(campos);
}

module.exports.eliminarObraProveedort = (idOBRAPROVEEDORT) => {
  return model.ObraProveedort.forge({ idOBRAPROVEEDORT: idOBRAPROVEEDORT }).destroy();
}

module.exports.consultarObraProveedorest=()=>{
  return model.ObraProveedort
  .query(function (qb) {
    qb.limit(100);
  }).orderBy('idOBRAPROVEEDORT', 'DESC').fetchAll({withRelated: ['obra','proveedort']});

}

module.exports.consultarObraProveedorestFiltros=(filtros)=>{
  return model.ObraProveedort
  .query(function (qb) {
    qb.limit(100);

    qb.innerJoin('OBRA', 'OBRA.idOBRA', 'OBRAPROVEEDORT.idOBRA');
    qb.innerJoin('PROVEEDORT', 'PROVEEDORT.idPROVEEDORT', 'OBRAPROVEEDORT.idPROVEEDORT')

    if(filtros.nomOBRA!=null){
      qb.where('OBRA.nomOBRA', 'like', '%'+filtros.nomOBRA+'%');
    }

    if (filtros.nomPROVEEDORT!=null){
    qb.where('PROVEEDORT.nomPROVEEDORT', 'like', '%'+filtros.nomPROVEEDORT+'%');
    }

    if (filtros.idOBRA!=null){
      qb.where('OBRA.idOBRA', '=', filtros.idOBRA);
    }

    if (filtros.idPROVEEDORT!=null){
        qb.where('PROVEEDORT.idPROVEEDORT', '=', filtros.idPROVEEDORT);
    }

  
    
  }).fetchAll({withRelated: ['obra','proveedort']});
}