
var dbController = require('./dbController');
var dateUtil = require('../utilities/dateUtil');
var config = require('../configuration/appConfig');





module.exports.consultarObrasFiltros  = (req, res) => {
	var filtros = req.jsonBody.filtros;
	dbController.consultarObrasFiltros(filtros)
		.then((obras) => {
			return res.json({ success: true, obras: obras.toJSON() });
		}).catch((error) => {
			console.log(error);
			return res.json({ success: false });
		});
}

module.exports.consultarObras  = (req, res) => {
	dbController.consultarObras()
		.then((obras) => {
			return res.json({ success: true, obras: obras.toJSON() });
		}).catch((error) => {
			console.log(error);
			return res.json({ success: false });
		});
}

module.exports.insertarObra = (req, res) => {
	
	var datosObra = req.jsonBody.datosObra;

	console.log('crearObra',datosObra);

	dbController.insertarObra(datosObra)
		.then((nuevaObra) => {
			return res.json({ success: true, obra: nuevaObra.toJSON() });
			//return res.json({ success: true });
		}).catch((error) => {
			console.log(error);
			return res.json({ success: false });
		});
}

module.exports.editarObra = (req, res) => {
	var camposEditados = req.jsonBody.campos;

	dbController.editarObra(req.jsonBody.idOBRA, camposEditados)
		.then(() => {
			return res.json({ success: true });
		}).catch((error) => {
			console.log(error);
			return res.json({ success: false });
		});
}

module.exports.eliminarObra = (req, res) => {

	dbController.eliminarObra(req.jsonBody.idOBRA)
		.then(() => {
			return res.json({ success: true });
		}).catch((error) => {
			console.log(error);
			return res.json({ success: false });
		});
}


module.exports.consultarClientesFiltros  = (req, res) => {
	var filtros = req.jsonBody.filtros;
	dbController.consultarClientesFiltros(filtros)
		.then((clientes) => {
			return res.json({ success: true, clientes: clientes.toJSON() });
		}).catch((error) => {
			console.log(error);
			return res.json({ success: false });
		});
}

module.exports.consultarClientes  = (req, res) => {
	dbController.consultarClientes()
		.then((clientes) => {
			return res.json({ success: true, clientes: clientes.toJSON() });
		}).catch((error) => {
			console.log(error);
			return res.json({ success: false });
		});
}

module.exports.insertarCliente = (req, res) => {
	console.log('crearCliente');
	var datoscliente = req.jsonBody.datosCliente;

	dbController.insertarCliente(datoscliente)
		.then((nuevoCliente) => {
			return res.json({ success: true, cliente: nuevoCliente.toJSON() });
			//return res.json({ success: true });
		}).catch((error) => {
			console.log(error);
			return res.json({ success: false });
		});
}

module.exports.editarCliente = (req, res) => {
	var camposEditados = req.jsonBody.campos;

	dbController.editarCliente(req.jsonBody.idCLIENTE, camposEditados)
		.then(() => {
			return res.json({ success: true });
		}).catch((error) => {
			console.log(error);
			return res.json({ success: false });
		});
}

module.exports.eliminarCliente= (req, res) => {

	dbController.eliminarCliente(req.jsonBody.idCLIENTE)
		.then(() => {
			return res.json({ success: true });
		}).catch((error) => {
			console.log(error);
			return res.json({ success: false });
		});
}


module.exports.consultarProveedoresFiltros  = (req, res) => {
	var filtros = req.jsonBody.filtros;
	dbController.consultarProveedoresFiltros(filtros)
		.then((proveedores) => {
			return res.json({ success: true, proveedores: proveedores.toJSON() });
		}).catch((error) => {
			console.log(error);
			return res.json({ success: false });
		});
}

module.exports.consultarProveedores  = (req, res) => {
	dbController.consultarProveedores()
		.then((proveedores) => {
			return res.json({ success: true, proveedores: proveedores.toJSON() });
		}).catch((error) => {
			console.log(error);
			return res.json({ success: false });
		});
}

module.exports.insertarProveedor = (req, res) => {
	console.log('crearProveedor');
	var datosProveedor = req.jsonBody.datosProveedor;

	dbController.insertarProveedor(datosProveedor)
		.then((nuevoProveedor) => {
			return res.json({ success: true, proveedor: nuevoProveedor.toJSON() });
			//return res.json({ success: true });
		}).catch((error) => {
			console.log(error);
			return res.json({ success: false });
		});
}

module.exports.editarProveedor = (req, res) => {
	var camposEditados = req.jsonBody.campos;

	dbController.editarProveedor(req.jsonBody.idPROVEEDOR, camposEditados)
		.then(() => {
			return res.json({ success: true });
		}).catch((error) => {
			console.log(error);
			return res.json({ success: false });
		});
}

module.exports.eliminarProveedor= (req, res) => {

	dbController.eliminarProveedor(req.jsonBody.idPROVEEDOR)
		.then(() => {
			return res.json({ success: true });
		}).catch((error) => {
			console.log(error);
			return res.json({ success: false });
		});
}


module.exports.consultarEquiposFiltros  = (req, res) => {
	var filtros = req.jsonBody.filtros;
	dbController.consultarEquiposFiltros(filtros)
		.then((equipos) => {
			return res.json({ success: true, equipos: equipos.toJSON() });
		}).catch((error) => {
			console.log(error);
			return res.json({ success: false });
		});
}

module.exports.consultarEquipos  = (req, res) => {
	dbController.consultarEquipos()
		.then((equipos) => {
			return res.json({ success: true, equipos: equipos.toJSON() });
		}).catch((error) => {
			console.log(error);
			return res.json({ success: false });
		});
}

module.exports.insertarEquipo = (req, res) => {
	console.log('crearEquipo');
	var datosEquipo = req.jsonBody.datosEquipo;

	dbController.insertarEquipo(datosEquipo)
		.then((nuevoEquipo) => {
			return res.json({ success: true, equipo: nuevoEquipo.toJSON() });
			//return res.json({ success: true });
		}).catch((error) => {
			console.log(error);
			return res.json({ success: false });
		});
}

module.exports.editarEquipo = (req, res) => {
	var camposEditados = req.jsonBody.campos;

	dbController.editarEquipo(req.jsonBody.idEQUIPO, camposEditados)
		.then(() => {
			return res.json({ success: true });
		}).catch((error) => {
			console.log(error);
			return res.json({ success: false });
		});
}

module.exports.eliminarEquipo= (req, res) => {

	dbController.eliminarEquipo(req.jsonBody.idEQUIPO)
		.then(() => {
			return res.json({ success: true });
		}).catch((error) => {
			console.log(error);
			return res.json({ success: false });
		});
}


module.exports.consultarMaterialesFiltros  = (req, res) => {
	var filtros = req.jsonBody.filtros;
	dbController.consultarMaterialesFiltros(filtros)
		.then((materiales) => {
			return res.json({ success: true, materiales: materiales.toJSON() });
		}).catch((error) => {
			console.log(error);
			return res.json({ success: false });
		});
}

module.exports.consultarMateriales  = (req, res) => {
	dbController.consultarMateriales()
		.then((materiales) => {
			return res.json({ success: true, materiales: materiales.toJSON() });
		}).catch((error) => {
			console.log(error);
			return res.json({ success: false });
		});
}

module.exports.insertarMaterial = (req, res) => {
	console.log('crearMaterial');
	var datosMaterial = req.jsonBody.datosMaterial;

	dbController.insertarMaterial(datosMaterial)
		.then((nuevoMaterial) => {
			return res.json({ success: true, material: nuevoMaterial.toJSON() });
			//return res.json({ success: true });
		}).catch((error) => {
			console.log(error);
			return res.json({ success: false });
		});
}

module.exports.editarMaterial = (req, res) => {
	var camposEditados = req.jsonBody.campos;

	dbController.editarMaterial(req.jsonBody.idMATERIAL, camposEditados)
		.then(() => {
			return res.json({ success: true });
		}).catch((error) => {
			console.log(error);
			return res.json({ success: false });
		});
}

module.exports.eliminarMaterial= (req, res) => {

	dbController.eliminarMaterial(req.jsonBody.idMATERIAL)
		.then(() => {
			return res.json({ success: true });
		}).catch((error) => {
			console.log(error);
			return res.json({ success: false });
		});
}


module.exports.consultarPropietariosFiltros  = (req, res) => {
	var filtros = req.jsonBody.filtros;
	dbController.consultarPropietariosFiltros(filtros)
		.then((propietarios) => {
			return res.json({ success: true, propietarios: propietarios.toJSON() });
		}).catch((error) => {
			console.log(error);
			return res.json({ success: false });
		});
}

module.exports.consultarPropietarios  = (req, res) => {
	dbController.consultarPropietarios()
		.then((propietarios) => {
			return res.json({ success: true, propietarios: propietarios.toJSON() });
		}).catch((error) => {
			console.log(error);
			return res.json({ success: false });
		});
}

module.exports.insertarPropietario = (req, res) => {
	console.log('crearPropietario');
	var datosPropietario = req.jsonBody.datosPropietario;

	dbController.insertarPropietario(datosPropietario)
		.then((nuevoPropietario) => {
			return res.json({ success: true, propietario: nuevoPropietario.toJSON() });
			//return res.json({ success: true });
		}).catch((error) => {
			console.log(error);
			return res.json({ success: false });
		});
}

module.exports.editarPropietario = (req, res) => {
	var camposEditados = req.jsonBody.campos;

	dbController.editarPropietario(req.jsonBody.idPROPIETARIO, camposEditados)
		.then(() => {
			return res.json({ success: true });
		}).catch((error) => {
			console.log(error);
			return res.json({ success: false });
		});
}

module.exports.eliminarPropietario= (req, res) => {

	dbController.eliminarPropietario(req.jsonBody.idPROPIETARIO)
		.then(() => {
			return res.json({ success: true });
		}).catch((error) => {
			console.log(error);
			return res.json({ success: false });
		});
}

module.exports.consultarControlMaterialesFiltros  = (req, res) => {
	var filtros = req.jsonBody.filtros;
	dbController.consultarControlMaterialesFiltros(filtros)
		.then((controlMateriales) => {
			return res.json({ success: true, controlMateriales: controlMateriales.toJSON() });
		}).catch((error) => {
			console.log(error);
			return res.json({ success: false });
		});
}

module.exports.consultarControlMateriales  = (req, res) => {
	dbController.consultarControlMateriales()
		.then((controlMateriales) => {
			return res.json({ success: true, controlMateriales: controlMateriales.toJSON() });
		}).catch((error) => {
			console.log(error);
			return res.json({ success: false });
		});
}

module.exports.insertarControlMateriales = (req, res) => {
	console.log('crearControlMateriales');
	var datosControlMateriales = req.jsonBody.datosControlMateriales;

	dbController.insertarControlMateriales(datosControlMateriales)
		.then((nuevoControlMateriales) => {
			return res.json({ success: true, controlMateriales: nuevoControlMateriales.toJSON() });
			//return res.json({ success: true });
		}).catch((error) => {
			console.log(error);
			return res.json({ success: false });
		});
}

module.exports.editarControlMateriales = (req, res) => {
	var camposEditados = req.jsonBody.campos;

	dbController.editarControlMateriales(req.jsonBody.idCONTROL_MATERIALES, camposEditados)
		.then(() => {
			return res.json({ success: true });
		}).catch((error) => {
			console.log(error);
			return res.json({ success: false });
		});
}

module.exports.eliminarControlMateriales= (req, res) => {

	dbController.eliminarControlMateriales(req.jsonBody.idCONTROL_MATERIALES)
		.then(() => {
			return res.json({ success: true });
		}).catch((error) => {
			console.log(error);
			return res.json({ success: false });
		});
}

module.exports.consultarReporteEquiposFiltros  = (req, res) => {
	var filtros = req.jsonBody.filtros;
	dbController.consultarReportesEquipoFiltros(filtros)
		.then((reporteEquipos) => {
			return res.json({ success: true, reporteEquipos: reporteEquipos.toJSON() });
		}).catch((error) => {
			console.log(error);
			return res.json({ success: false });
		});
}

module.exports.consultarReporteEquipos  = (req, res) => {
	dbController.consultarReporteEquipos()
		.then((reporteEquipos) => {
			return res.json({ success: true, reporteEquipos: reporteEquipos.toJSON() });
		}).catch((error) => {
			console.log(error);
			return res.json({ success: false });
		});
}

module.exports.insertarReporteEquipos = (req, res) => {
	console.log('crearReporteEquipos');
	var datosReporteEquipo = req.jsonBody.datosReporteEquipo;

	dbController.insertarReporteEquipo(datosReporteEquipo)
		.then((nuevoReporteEquipos) => {
			return res.json({ success: true, reporteEquipos: nuevoReporteEquipos.toJSON() });
			//return res.json({ success: true });
		}).catch((error) => {
			console.log(error);
			return res.json({ success: false });
		});
}

module.exports.editarReporteEquipos = (req, res) => {
	var camposEditados = req.jsonBody.campos;

	dbController.editarReporteEquipo(req.jsonBody.idREPORTE_EQUIPO, camposEditados)
		.then(() => {
			return res.json({ success: true });
		}).catch((error) => {
			console.log(error);
			return res.json({ success: false });
		});
}

module.exports.eliminarReporteEquipos= (req, res) => {

	dbController.eliminarReporteEquipo(req.jsonBody.idREPORTE_EQUIPO)
		.then(() => {
			return res.json({ success: true });
		}).catch((error) => {
			console.log(error);
			return res.json({ success: false });
		});
}

module.exports.consultarDetReporteEquipos  = (req, res) => {
	dbController.consultarDetReporteEquipo(req.jsonBody.idREPORTE_EQUIPO)
		.then((detReporteEquipos) => {
			return res.json({ success: true, detReporteEquipos: detReporteEquipos.toJSON() });
		}).catch((error) => {
			console.log(error);
			return res.json({ success: false });
		});
}

module.exports.insertarDetReporteEquipos = (req, res) => {
	console.log('crearDetReporteEquipos');
	var datosDetReporteEquipo = req.jsonBody.datosDetReporteEquipo;

	dbController.insertarDetReporteEquipo(datosDetReporteEquipo)
		.then((nuevoDetReporteEquipo) => {
			return res.json({ success: true, detReporteEquipo: nuevoDetReporteEquipo.toJSON() });
			//return res.json({ success: true });
		}).catch((error) => {
			console.log(error);
			return res.json({ success: false });
		});
}

module.exports.editarDetReporteEquipos = (req, res) => {
	var camposEditados = req.jsonBody.campos;

	dbController.editarDetReporteEquipos(req.jsonBody.idDET_REPORTE_EQUIPOS, camposEditados)
		.then(() => {
			return res.json({ success: true });
		}).catch((error) => {
			console.log(error);
			return res.json({ success: false });
		});
}

module.exports.eliminarDetReporteEquipos= (req, res) => {

	dbController.eliminarDetReporteEquipo(req.jsonBody.idREPORTE_EQUIPO)
		.then(() => {
			return res.json({ success: true });
		}).catch((error) => {
			console.log(error);
			return res.json({ success: false });
		});
}

module.exports.consultarUsuarios  = (req, res) => {
	dbController.consultarUsuarios()
		.then((usuarios) => {
			return res.json({ success: true, usuarios: usuarios.toJSON() });
		}).catch((error) => {
			console.log(error);
			return res.json({ success: false });
		});
}

module.exports.consultarRoles  = (req, res) => {
	dbController.consultarRoles()
		.then((roles) => {
			return res.json({ success: true, roles: roles.toJSON() });
		}).catch((error) => {
			console.log(error);
			return res.json({ success: false });
		});
}



module.exports.consultarUsuarioFiltros  = (req, res) => {
	var filtros = req.jsonBody.filtros;
	dbController.consultarUsuarioFiltros(filtros)
		.then((usuarios) => {
			return res.json({ success: true, usuarios: usuarios.toJSON() });
		}).catch((error) => {
			console.log(error);
			return res.json({ success: false });
		});
}



module.exports.insertarUsuario = (req, res) => {
	console.log('crearUsuario');
	var datosUsuario = req.jsonBody.datosUsuario;

	dbController.insertarUsuario(datosUsuario)
		.then((nuevoUsuario) => {
			return res.json({ success: true, usuario: nuevoUsuario.toJSON() });
			//return res.json({ success: true });
		}).catch((error) => {
			console.log(error);
			return res.json({ success: false });
		});
}

module.exports.editarUsuario = (req, res) => {
	var camposEditados = req.jsonBody.campos;

	dbController.editarUsuario(req.jsonBody.idUSUARIO, camposEditados)
		.then(() => {
			return res.json({ success: true });
		}).catch((error) => {
			console.log(error);
			return res.json({ success: false });
		});
}

module.exports.eliminarUsuario= (req, res) => {

	dbController.eliminarUsuario(req.jsonBody.idUSUARIO)
		.then(() => {
			return res.json({ success: true });
		}).catch((error) => {
			console.log(error);
			return res.json({ success: false });
		});
}





module.exports.consultarActividadesFiltros  = (req, res) => {
	var filtros = req.jsonBody.filtros;
	dbController.consultarActividadesFiltros(filtros)
		.then((actividades) => {
			return res.json({ success: true, actividades: actividades.toJSON() });
		}).catch((error) => {
			console.log(error);
			return res.json({ success: false });
		});
}

module.exports.consultarActividades  = (req, res) => {
	dbController.consultarActividades()
		.then((actividades) => {
			return res.json({ success: true, actividades: actividades.toJSON() });
		}).catch((error) => {
			console.log(error);
			return res.json({ success: false });
		});
}

module.exports.insertarActividad = (req, res) => {
	console.log('crearActividad');
	var datosActividad = req.jsonBody.datosActividad;

	dbController.insertarActividad(datosActividad)
		.then((nuevaActividad) => {
			return res.json({ success: true, actividad: nuevaActividad.toJSON() });
			//return res.json({ success: true });
		}).catch((error) => {
			console.log(error);
			return res.json({ success: false });
		});
}

module.exports.editarActividad = (req, res) => {
	var camposEditados = req.jsonBody.campos;

	dbController.editarActividad(req.jsonBody.idACTIVIDAD, camposEditados)
		.then(() => {
			return res.json({ success: true });
		}).catch((error) => {
			console.log(error);
			return res.json({ success: false });
		});
}

module.exports.eliminarActividad= (req, res) => {

	dbController.eliminarActividad(req.jsonBody.idACTIVIDAD)
		.then(() => {
			return res.json({ success: true });
		}).catch((error) => {
			console.log(error);
			return res.json({ success: false });
		});
}


module.exports.consultarProveedorestFiltros  = (req, res) => {
	var filtros = req.jsonBody.filtros;
	dbController.consultarProveedorestFiltros(filtros)
		.then((proveedorest) => {
			return res.json({ success: true, proveedorest: proveedorest.toJSON() });
		}).catch((error) => {
			console.log(error);
			return res.json({ success: false });
		});
}

module.exports.consultarProveedorest  = (req, res) => {
	dbController.consultarProveedorest()
		.then((proveedorest) => {
			return res.json({ success: true, proveedorest: proveedorest.toJSON() });
		}).catch((error) => {
			console.log(error);
			return res.json({ success: false });
		});
}

module.exports.insertarProveedort = (req, res) => {
	console.log('crearProveedort');
	var datosProveedort = req.jsonBody.datosProveedort;

	dbController.insertarProveedort(datosProveedort)
		.then((nuevoProveedort) => {
			return res.json({ success: true, proveedort: nuevoProveedort.toJSON() });
			//return res.json({ success: true });
		}).catch((error) => {
			console.log(error);
			return res.json({ success: false });
		});
}

module.exports.editarProveedort = (req, res) => {
	var camposEditados = req.jsonBody.campos;

	dbController.editarProveedort(req.jsonBody.idPROVEEDORT, camposEditados)
		.then(() => {
			return res.json({ success: true });
		}).catch((error) => {
			console.log(error);
			return res.json({ success: false });
		});
}

module.exports.eliminarProveedort= (req, res) => {

	dbController.eliminarProveedort(req.jsonBody.idPROVEEDORT)
		.then(() => {
			return res.json({ success: true });
		}).catch((error) => {
			console.log(error);
			return res.json({ success: false });
		});
}



module.exports.consultarObraProveedorestFiltros  = (req, res) => {
	var filtros = req.jsonBody.filtros;
	dbController.consultarObraProveedorestFiltros(filtros)
		.then((obraProveedorest) => {
			return res.json({ success: true, obraProveedorest: obraProveedorest.toJSON() });
		}).catch((error) => {
			console.log(error);
			return res.json({ success: false });
		});
}

module.exports.consultarObraProveedorest  = (req, res) => {
	dbController.consultarObraProveedorest()
		.then((obraProveedorest) => {
			return res.json({ success: true, obraProveedorest: obraProveedorest.toJSON() });
		}).catch((error) => {
			console.log(error);
			return res.json({ success: false });
		});
}

module.exports.insertarObraProveedort = (req, res) => {
	console.log('crearObraProveedort');
	var datosObraProveedort = req.jsonBody.datosObraProveedort;

	dbController.insertarObraProveedort(datosObraProveedort)
		.then((nuevoObraProveedort) => {
			return res.json({ success: true, obraProveedort: nuevoObraProveedort.toJSON() });
			//return res.json({ success: true });
		}).catch((error) => {
			console.log(error);
			return res.json({ success: false });
		});
}

module.exports.editarObraProveedort = (req, res) => {
	var camposEditados = req.jsonBody.campos;

	console.log(camposEditados);
			console.log(req.jsonBody.idOBRAPROVEEDORT);

	dbController.editarObraProveedort(req.jsonBody.idOBRAPROVEEDORT, camposEditados)
		.then(() => {
			
			return res.json({ success: true });
		}).catch((error) => {
			console.log(error);
			return res.json({ success: false });
		});
}

module.exports.eliminarObraProveedort= (req, res) => {

	dbController.eliminarObraProveedort(req.jsonBody.idOBRAPROVEEDORT)
		.then(() => {
			return res.json({ success: true });
		}).catch((error) => {
			console.log(error);
			return res.json({ success: false });
		});
}




function obtenerValorEstadoLicenciabyName(name){ 
	var lista = config.licencias;

	for (let obj of lista){ 

		if (obj.string == name){ 
			return obj.valor;
		}

	}
}
