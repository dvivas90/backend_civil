module.exports.parser = (req, res, next) =>
{
  var data = "";

  req.on('data', (chunk) => { data += chunk});

  req.on('end', () =>
  {
    req.rawBody = data;
    req.jsonBody = JSON.parse(data);
    next();
  })
};
