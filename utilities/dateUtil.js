var dateFormat=require('dateformat');

/**
 * Obtiene la fecha y hora actuales con el metodo nativo y los formatea
 * para devolver un tipo de fecha/hora aceptada por la BD 
 * @returns {string} fecha en formato "yyyy-mm-dd HH:MM:ss" 
 */
module.exports.getCurrentDate=()=>{
	var now = new Date();
	now.setHours(now.getHours()-1);
	var formattedDate=dateFormat(now, "yyyy-mm-dd HH:MM:ss");
	return formattedDate;

}

module.exports.formatDate=(date)=>{
	var formattedDate=dateFormat(date, "yyyy-mm-dd HH:MM:ss o");
	console.log(formattedDate);
	return formattedDate;
}

module.exports.addDays=(date, days)=>{
	var result = new Date(date);
	result.setDate(result.getDate() + days);
	return result;
  }