function Cliente(userId,viaje, tiempo, timer, socket, finalizo){
    //codigo de la persona
    this.userId=userId;
    //codigo del recorrido actual
    this.viaje=viaje;
    //tiempo estimado de viaje
    this.tiempo=tiempo;
    //contador intentos fallidos de código (normal/panico)
    this.intentos=0;
    //timer del tiempo de viaje
    this.timer=timer;
    //socket de conexion
    this.socket=socket;
    //Numero de conexiones establecidas para el recorrido
    this.conexiones=1;
    //socket/conexion activa
    this.online=true;
    //variable que indica si el viaje finalizo por el usuario
    this.finalizo = finalizo;
}

module.exports = Cliente;