var cors = require('cors');
var express = require('express');
var app = express();
var parser = require('../utilities/jsonparser');
var restController=require('../controllers/restController');

var config = require('../configuration/appConfig');
var router = express.Router();

var server=require('http').Server(app);


router.all('*', cors());

router.get('/',cors(),function(req,res){
    return res.send("Testing server");
});

router.post('/insertarObra', cors(), parser.parser, restController.insertarObra);
router.post('/editarObra', cors(), parser.parser, restController.editarObra);
router.post('/eliminarObra', cors(), parser.parser, restController.eliminarObra);
router.post('/consultarObras', cors(), parser.parser, restController.consultarObras);
router.post('/consultarObrasFiltros', cors(), parser.parser, restController.consultarObrasFiltros);

router.post('/insertarCliente', cors(), parser.parser, restController.insertarCliente);
router.post('/editarCliente', cors(), parser.parser, restController.editarCliente);
router.post('/eliminarCliente', cors(), parser.parser, restController.eliminarCliente);
router.post('/consultarClientes', cors(), parser.parser, restController.consultarClientes);
router.post('/consultarClientesFiltros', cors(), parser.parser, restController.consultarClientesFiltros);

router.post('/insertarProveedor', cors(), parser.parser, restController.insertarProveedor);
router.post('/editarProveedor', cors(), parser.parser, restController.editarProveedor);
router.post('/eliminarProveedor', cors(), parser.parser, restController.eliminarProveedor);
router.post('/consultarProveedores', cors(), parser.parser, restController.consultarProveedores);
router.post('/consultarProveedoresFiltros', cors(), parser.parser, restController.consultarProveedoresFiltros);

router.post('/insertarEquipo', cors(), parser.parser, restController.insertarEquipo);
router.post('/editarEquipo', cors(), parser.parser, restController.editarEquipo);
router.post('/eliminarEquipo', cors(), parser.parser, restController.eliminarEquipo);
router.post('/consultarEquipos', cors(), parser.parser, restController.consultarEquipos);
router.post('/consultarEquiposFiltros', cors(), parser.parser, restController.consultarEquiposFiltros);

router.post('/insertarMaterial', cors(), parser.parser, restController.insertarMaterial);
router.post('/editarMaterial', cors(), parser.parser, restController.editarMaterial);
router.post('/eliminarMaterial', cors(), parser.parser, restController.eliminarMaterial);
router.post('/consultarMateriales', cors(), parser.parser, restController.consultarMateriales);
router.post('/consultarMaterialesFiltros', cors(), parser.parser, restController.consultarMaterialesFiltros);

router.post('/insertarPropietario', cors(), parser.parser, restController.insertarPropietario);
router.post('/editarPropietario', cors(), parser.parser, restController.editarPropietario);
router.post('/eliminarPropietario', cors(), parser.parser, restController.eliminarPropietario);
router.post('/consultarPropietarios', cors(), parser.parser, restController.consultarPropietarios);
router.post('/consultarPropietariosFiltros', cors(), parser.parser, restController.consultarPropietariosFiltros);

router.post('/insertarControlMateriales', cors(), parser.parser, restController.insertarControlMateriales);
router.post('/editarControlMateriales', cors(), parser.parser, restController.editarControlMateriales);
router.post('/eliminarControlMateriales', cors(), parser.parser, restController.eliminarControlMateriales);
router.post('/consultarControlMateriales', cors(), parser.parser, restController.consultarControlMateriales);
router.post('/consultarControlMaterialesFiltros', cors(), parser.parser, restController.consultarControlMaterialesFiltros);


router.post('/insertarReporteEquipos', cors(), parser.parser, restController.insertarReporteEquipos);
router.post('/editarReporteEquipos', cors(), parser.parser, restController.editarReporteEquipos);
router.post('/eliminarReporteEquipos', cors(), parser.parser, restController.eliminarReporteEquipos);
router.post('/consultarReporteEquipos', cors(), parser.parser, restController.consultarReporteEquipos);
router.post('/consultarReporteEquiposFiltros', cors(), parser.parser, restController.consultarReporteEquiposFiltros);

router.post('/insertarDetReporteEquipos', cors(), parser.parser, restController.insertarDetReporteEquipos);
router.post('/editarDetReporteEquipos', cors(), parser.parser, restController.editarDetReporteEquipos);
router.post('/eliminarDetReporteEquipos', cors(), parser.parser, restController.eliminarDetReporteEquipos);
router.post('/consultarDetReporteEquipos', cors(), parser.parser, restController.consultarDetReporteEquipos);

router.post('/insertarUsuario', cors(), parser.parser, restController.insertarUsuario);
router.post('/editarUsuario', cors(), parser.parser, restController.editarUsuario);
router.post('/eliminarUsuario', cors(), parser.parser, restController.eliminarUsuario);
router.post('/consultarUsuarios', cors(), parser.parser, restController.consultarUsuarios);
router.post('/consultarUsuarioFiltros', cors(), parser.parser, restController.consultarUsuarioFiltros);

router.post('/consultarRoles', cors(), parser.parser, restController.consultarRoles);

router.post('/insertarActividad', cors(), parser.parser, restController.insertarActividad);
router.post('/editarActividad', cors(), parser.parser, restController.editarActividad);
router.post('/eliminarActividad', cors(), parser.parser, restController.eliminarActividad);
router.post('/consultarActividades', cors(), parser.parser, restController.consultarActividades);
router.post('/consultarActividadesFiltros', cors(), parser.parser, restController.consultarActividadesFiltros);

router.post('/insertarProveedort', cors(), parser.parser, restController.insertarProveedort);
router.post('/editarProveedort', cors(), parser.parser, restController.editarProveedort);
router.post('/eliminarProveedort', cors(), parser.parser, restController.eliminarProveedort);
router.post('/consultarProveedorest', cors(), parser.parser, restController.consultarProveedorest);
router.post('/consultarProveedorestFiltros', cors(), parser.parser, restController.consultarProveedorestFiltros);


router.post('/insertarObraProveedort', cors(), parser.parser, restController.insertarObraProveedort);
router.post('/editarObraProveedort', cors(), parser.parser, restController.editarObraProveedort);
router.post('/eliminarObraProveedort', cors(), parser.parser, restController.eliminarObraProveedort);
router.post('/consultarObraProveedorest', cors(), parser.parser, restController.consultarObraProveedorest);
router.post('/consultarObraProveedorestFiltros', cors(), parser.parser, restController.consultarObraProveedorestFiltros);

app.use('/',router);
server.listen(config.serverPort, '0.0.0.0', function () {
  console.log("Servidor corriendo por el puerto " + config.serverPort);
});
